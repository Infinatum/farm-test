# Farm Test  #
 
The Mod Which improves farming Minetest (we aim to make the farming as realistic as minetest allows us

### About the Mod ###

A up and coming mod which improves the farming in minetest, as well as adding new things and plants (COMING SOON) 

In Farm Test we try to make farming as realistic as possible. Because of this we are doing things differently to other games or mods. This will make things more existing since they are different ways to get the product from the plant. 

Current version pre alpha

**Farm Test is still under construction meaning it is not at a playable state. This is just for developers or people who want to look at the current state of the mod**   

### How To Install ###

## Linux ##
1. Go to your Home directory
2. You should see a folder called minetest. If not the folder is probely hidden. To get to it type "/.minetest/" in the navigation bar.
3. You should now be in a folder called ".minetest". in this folder you should see another folder called "mods". If you dont see this make a new folder called "mods".
4.   Copy and Paste the mod into the "mods" folder. (make sure that it is extracted from the zip file". 

## Windows ##
1. Locate where the game has been saved. if you don't know where it has been saved right click the minetest icon and press "Open file location"
2. when you are in the minetest folder you should see a folder called "mods", as well as other folders like "bin", "textures" , "games", ect. 
3. Copy and Paste the mod into the "mods" folder. (make sure that it is extracted from the zip file". 

### More Info ###

[Website](http://thatraspberrypiserver.raspberryip.com/Infinatum_Minetest/farm_test.html)
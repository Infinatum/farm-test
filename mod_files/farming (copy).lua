--======================--
-- Items                --
--======================--


minetest.register_craftitem("farm_test:red_tomato", {
	description = "Red Tomato",
	inventory_image = "farm_test_tomato_red.png",
}) 
--======================--
-- Blocks               --
--======================--

--======================--
-- Plants               --
--======================--

-----------------------
-- Tomatoes growing  --
-----------------------
-- tomato --

minetest.register_node("farm_test:tomato_9", {
	description = "tomatos_9",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:tomatos",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_tomato_9.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_leaves_defaults(),
})

minetest.register_node("farm_test:tomato_9a", {
	description = "tomatos_9a",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:red_tomatos",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_tomato_9a.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_leaves_defaults(),
	on_punch = function(pos, node, puncher)
			local tool = puncher:get_wielded_item():get_name()
			if tool and tool == "" then
				node.name = "farm_test:tomato_9"
				minetest.env:set_node(pos, node)
				puncher:get_inventory():add_item("main", ItemStack("farm_test:red_tomato"))
			end
		end
})

-- Growing -- 
minetest.register_craftitem("farm_test:tomato", {
	description = "Red Tomato",
	inventory_image = "farm_test_tomato_red.png",
}) 



farming.register_plant("farm_test:tomato", {
description = "Tomato Seed",
inventory_image = "farm_test_tomato_green.png",
drop = "farm_test:seed_tomato",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,
fertility = {"grassland"}
})

minetest.register_abm({
	nodenames = {"farm_test:tomato_8", "farm_test:tomato_9"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
		pos.y = pos.y+1
		local nn = minetest.get_node(pos).name
		pos.y = pos.y-1
		if minetest.registered_nodes[nn] and minetest.registered_nodes[nn].walkable then
			minetest.set_node(pos, {name="farm_test:tomato_8"})
		end
		-- check if there is air nearby
		if minetest.find_node_near(pos, 5, {"air"}) then
			if node.name == "farm_test:tomato_8" then
				minetest.set_node(pos, {name="farm_test:tomato_9"})
			end
		end
	end,
})

minetest.register_abm({
	nodenames = {"farm_test:tomato_9", "farm_test:tomato_9a"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
		pos.y = pos.y+1
		local nn = minetest.get_node(pos).name
		pos.y = pos.y-1
		if minetest.registered_nodes[nn] and minetest.registered_nodes[nn].walkable then
			minetest.set_node(pos, {name="farm_test:tomato_8"})
		end
		-- check if there is air nearby
		if minetest.find_node_near(pos, 5, {"air"}) then
			if node.name == "farm_test:tomato_9" then
				minetest.set_node(pos, {name="farm_test:tomato_9a"})
			end
		end
	end,
})

-- grow up up and away!--

minetest.register_abm({
	nodenames = {"farm_test:tomato_9a"},
	neighbors = {"farming:soil_wet"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
		pos.y = pos.y-1
		local name = minetest.get_node(pos).name
		if name == "farming:soil_wet" or name == "default:dirt_with_grass" or name == "farming:soil" or name == "default:dirt" then
			pos.y = pos.y+1
			local height = 0
			while minetest.get_node(pos).name == "farm_test:tomato_9a" and height < 4 do
				height = height+1
				pos.y = pos.y+1
			end
			if height < 4 then
				if minetest.get_node(pos).name == "air" then
					minetest.set_node(pos, {name="farm_test:tomato_9a"})
				end
			end
		end
	end,
})

--[[
for i=1,8 do
	local drop = {
		items = {
			{items = {'farm_test:tomato'},rarity=9-i},
			{items = {'farm_test:tomato'},rarity=18-i*2},
			{items = {'farm_test:tomato'},rarity=27-i*3},
			{items = {'farm_test:seed_tomato'},rarity=9-i},
			{items = {'farm_test:seed_tomato'},rarity=18-i*2},
			{items = {'farm_test:seed_tomato'},rarity=27-i*3},
			--{items = {'farm_test:crop'},rarity=1-i},--
		}
	}
	minetest.register_node("farm_test:tomato_"..i, {
		drawtype = "plantlike",
		tiles = {"farm_test_tomato_"..i..".png"},
		paramtype = "light",
		walkable = false,
		is_ground_content = true,
		drop = drop,it
		groups = {snappy=3,flammable=2,plant=1,tomato=i,not_in_creative_inventory=1},
		sounds = default.node_sound_leaves_defaults(),
	})
end

minetest.register_abm({
	nodenames = {"group:tomato"},
	neighbors = {"group:soil"},
	interval = 80,
	chance = 2,
	action = function(pos, node)
		-- return if already full grown
		if minetest.get_item_group(node.name, "tomato") == 8 then
			return
		end
		
		-- check if on wet soil
		pos.y = pos.y-1
		local n = minetest.get_node(pos)
		if minetest.get_item_group(n.name, "soil") < 3 then
			return
		end
		pos.y = pos.y+1
		
		-- check light
		if not minetest.get_node_light(pos) then
			return
		end
		if minetest.get_node_light(pos) < 13 then
			return
		end
		
		-- grow
		local height = minetest.get_item_group(node.name, "tomato") + 1
		minetest.set_node(pos, {name="farm_test:tomato_"..height})
	end
})
]]
-------=============================================[--]--	
print("Farm_test: farming.lua                       [ok]")




-- oats --

farming.register_plant("farm_test:potato", {
description = "Potato",
inventory_image = "farm_test_potato_seed.png",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,
fertility = {"grassland"}
})

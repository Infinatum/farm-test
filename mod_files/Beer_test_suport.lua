--======================--
-- beer_test			--
--======================--

-- Beer Test is need for this to work --


minetest.register_node("farm_test:test", {
	description = "Test Item Beer Test",
	paramtype = "light",
	walkable = false,
	drop = "Sweet Corn Seeds",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"test.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
})
--======================--
-- Plants               --
--======================--




------------------------
-- sweetcorn growing  --
------------------------

farming.register_plant("farm_test:sweet_corn", {
description = "Sweet Corn Seeds",
inventory_image = "farm_test_sweet_corn_seed.png",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,

fertility = {"grassland"}

})


minetest.register_node("farm_test:sweet_corn_9", {
	description = "farm_test:sweet_corn_9",
	paramtype = "light",
	walkable = false,
	drop = "Sweet Corn Seeds",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_sweet_corn.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
})

minetest.register_node("farm_test:sweet_corn_9a", {
	description = "farm_test:sweet_corn_9a",
	paramtype = "light",
	walkable = false,
	drop = "Sweet Corn Seeds",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_sweet_corn.png^farm_test_plant_sweetcorn.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
	on_punch = function(pos, node, puncher)
			local tool = puncher:get_wielded_item():get_name()
			if tool and tool == "" then
				node.name = "farm_test:sweet_corn_9"
				minetest.env:set_node(pos, node)
				puncher:get_inventory():add_item("main", ItemStack("farm_test:sweet_corn"))
			end
		end
})

minetest.register_node("farm_test:sweet_corn_tassel", {
	description = "farm_test:sweet_corn_tassel",
	paramtype = "light",
	walkable = false,
	drop = "Sweet Corn Seeds",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_sweet_corn_tassel.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
})

-- switch plant stages --

minetest.register_abm({
	nodenames = {"farm_test:sweet_corn_8"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:sweet_corn_9"})
	end,
})

minetest.register_abm({
	nodenames = {"farm_test:sweet_corn_9"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:sweet_corn_9a"})
	end,
})


-- grow up up and away!--

minetest.register_abm({
	nodenames = {"farm_test:sweet_corn_9a"},
	neighbors = {"farming:soil_wet"},
	interval = 2,
	chance = 5,
	action = function(pos, node)
		pos.y = pos.y-1
		local name = minetest.get_node(pos).name
		if name == "farming:soil_wet" or name == "default:dirt_with_grass" or name == "farming:soil" or name == "default:dirt" then
			pos.y = pos.y+1
			local height = 0
			while minetest.get_node(pos).name == "farm_test:sweet_corn_9a" and height < 1 do
				height = height+1
				pos.y = pos.y+1
			end
			if height < 4 then
				if minetest.get_node(pos).name == "air" then
					minetest.set_node(pos, {name="farm_test:sweet_corn_tassel"})
				end
			end
		end
	end,
})

--======================--
-- Items                --
--======================--


minetest.register_craftitem("farm_test:red_tomato", {
	description = "Red Tomato",
	inventory_image = "farm_test_tomato_red.png",
	on_use = minetest.item_eat(2),
})

minetest.register_craftitem("farm_test:green_tomato", {
	description = "Green Tomato",
	inventory_image = "farm_test_tomato_green.png",
	on_use = minetest.item_eat(1),
})

--======================--
-- Crafts               --
--======================--


minetest.register_craft({
	output = "farm_test:seed_tomato",
	recipe = {
		{"farm_test:red_tomato"},

	}
})

minetest.register_craft({
	output = "farm_test:seed_tomato",
	recipe = {
		{"farm_test:green_tomato"},

	}
})

--======================--
-- Plants               --
--======================--

-----------------------
-- Tomatoes growing  --
-----------------------
-- tomato --

--
-- Place seeds
--
local function place_seed(itemstack, placer, pointed_thing, plantname)
	local pt = pointed_thing
	-- check if pointing at a node
	if not pt then
		return
	end
	if pt.type ~= "node" then
		return
	end
	
	local under = minetest.get_node(pt.under)
	local above = minetest.get_node(pt.above)
	
	-- return if any of the nodes is not registered
	if not minetest.registered_nodes[under.name] then
		return
	end
	if not minetest.registered_nodes[above.name] then
		return
	end
	
	-- check if pointing at the top of the node
	if pt.above.y ~= pt.under.y+1 then
		return
	end
	
	-- check if you can replace the node above the pointed node
	if not minetest.registered_nodes[above.name].buildable_to then
		return
	end
	
	-- check if pointing at soil
	if minetest.get_item_group(under.name, "soil") <= 1 then
		return
	end
	
	-- add the node and remove 1 item from the itemstack
	minetest.add_node(pt.above, {name=plantname})
	if not minetest.setting_getbool("creative_mode") then
		itemstack:take_item()
	end
	return itemstack
end

minetest.register_craftitem("farm_test:seed_tomato", {
	description = "tomato Seed",
	inventory_image = "farm_test_tomato_seed.png",
	on_place = function(itemstack, placer, pointed_thing)
		return place_seed(itemstack, placer, pointed_thing, "farm_test:tomato_1")
	end,
})



for i=1,8 do
	local drop = {
		items = {
			--{items = {'farm_test:yellow_tomato'},rarity=18-i},
			--{items = {'farm_test:yellow_tomato'},rarity=27-i*2},
			{items = {'farm_test:green_tomato'},rarity=9-i},
			{items = {'farm_test:green_tomato'},rarity=18-i*2},
			{items = {'farm_test:green_tomato'},rarity=27-i*3},
		}
	}
	minetest.register_node("farm_test:tomato_"..i, {
		drawtype = "plantlike",
		tiles = {"farm_test_tomato_"..i..".png"},
		paramtype = "light",
		walkable = false,
		buildable_to = true,
		is_ground_content = true,
		drop = drop,
		selection_box = {
			type = "fixed",
			fixed = {-0.5, -0.5, -0.5, 0.5, -5/16, 0.5},
		},
		groups = {snappy=3,flammable=2,plant=1,tomato=i,not_in_creative_inventory=1,attached_node=1},
		sounds = default.node_sound_leaves_defaults(),
	})
end

minetest.register_abm({
	nodenames = {"group:tomato"},
	neighbors = {"group:soil"},
	interval = 80,
	chance = 2,
	action = function(pos, node)
		-- return if already full grown
		if minetest.get_item_group(node.name, "tomato") == 8 then
			return
		end
		
		-- check if on wet soil
		pos.y = pos.y-1
		local n = minetest.get_node(pos)
		if minetest.get_item_group(n.name, "soil") < 3 then
			return
		end
		pos.y = pos.y+1
		
		-- check light
		if not minetest.get_node_light(pos) then
			return
		end
		if minetest.get_node_light(pos) < 13 then
			return
		end
		
		-- grow
		local height = minetest.get_item_group(node.name, "tomato") + 1
		minetest.set_node(pos, {name="farm_test:tomato_"..height})
	end
})

-- tomatos 9 and 9a --

minetest.register_node("farm_test:tomato_9", {
	description = "farm_test:tomato_9",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:green_tomato",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_tomato.png^farm_test_plant_green_tomato.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
	on_punch = function(pos, node, puncher)
			local tool = puncher:get_wielded_item():get_name()
			if tool and tool == "" then
				node.name = "farm_test:tomato_9b"
				minetest.env:set_node(pos, node)
				puncher:get_inventory():add_item("main", ItemStack("farm_test:green_tomato"))
			end
		end
})

minetest.register_node("farm_test:tomato_9a", {
	description = "farm_test:tomato_9a",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:tomato",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_tomato.png^farm_test_plant_red_tomato.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}
		},
	},
	on_punch = function(pos, node, puncher)
			local tool = puncher:get_wielded_item():get_name()
			if tool and tool == "" then
				node.name = "farm_test:tomato_9b"
				minetest.env:set_node(pos, node)
				puncher:get_inventory():add_item("main", ItemStack("farm_test:red_tomato"))
			end
		end
})


minetest.register_node("farm_test:tomato_9b", {
	description = "farm_test:tomato_9b",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:green_tomato",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_tomato.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}, 
		},
	},
})

minetest.register_node("farm_test:tomato_9c", {
	description = "farm_test:tomato_9c",
	paramtype = "light",
	walkable = false,
	drop = "farm_test:green_tomato",
	drawtype = "plantlike",
	paramtype2 = "facedir",
	tiles = {"farm_test_plant_tomato.png^farm_test_plant_flower_tomato.png"},
	groups = {chopspy=2, oddly_breakable_by_hand=3, flammable=2, plant=1},
	sounds = default.node_sound_wood_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1, 0.5, -0.1, 0.1, -0.5, 0.1}, 
		},
	},
})



-- Grow Plant --

-- grow tomatos on plant -- 

-- farming:add_plant("farm_test:tomato_8", {"farm_test:tomato_9"}, 80, 20)




-- Regrowing tomatos when picked -- 

-- old crappy code --

--[[ ]]--

minetest.register_abm({
	nodenames = {"farm_test:tomato_8"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:tomato_9"})
	end,
})

minetest.register_abm({
	nodenames = {"farm_test:tomato_9"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:tomato_9a"})
	end,
})

minetest.register_abm({
	nodenames = {"farm_test:tomato_9b"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:tomato_9c"})
	end,
})

minetest.register_abm({
	nodenames = {"farm_test:tomato_9c"},
	neighbors = {"air"},
	interval = 15,
	chance = 5,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node(pos, {name = "farm_test:tomato_9"})
	end,
})

--[[ ]]--

-- grow up up and away!--

minetest.register_abm({
	nodenames = {"farm_test:tomato_9a"},
	neighbors = {"farming:soil_wet"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
		pos.y = pos.y-1
		local name = minetest.get_node(pos).name
		if name == "farming:soil_wet" or name == "default:dirt_with_grass" or name == "farming:soil" or name == "default:dirt" then
			pos.y = pos.y+1
			local height = 0
			while minetest.get_node(pos).name == "farm_test:tomato_9a" and height < 2 do
				height = height+1
				pos.y = pos.y+1
			end
			if height < 4 then
				if minetest.get_node(pos).name == "air" then
					minetest.set_node(pos, {name="farm_test:tomato_9"})
				end
			end
		end
	end,
})

-------=============================================[--]--	
print("Farm_test: farm_test.lua                       [ok]")




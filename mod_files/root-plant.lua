--======================--
-- Carrots				--
--======================--

farming.register_plant("farm_test:carrot", {
description = "Carrot Sprout",
inventory_image = "farm_test_carrot_sprouted.png",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,
fertility = {"grassland"}
})

--======================--
-- Potatos 				--
--======================--


farming.register_plant("farm_test:potato", {
description = "Potato Sprout",
inventory_image = "farm_test_potato_sprouted.png",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,
fertility = {"grassland"}
})


minetest.register_node("farm_test:soil_potato", {
	description = "Wet With Potatos",
	tiles = {"default_dirt.png^farming_soil_wet.png", "default_dirt.png^farming_soil_potato_side.png"},
	drop = "default:dirt",
	is_ground_content = true,
	groups = {crumbly=3, not_in_creative_inventory=1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_abm({
	nodenames = {"farming:soil_wet"},
	neighbors = {"farm_test:potato_7", "farm_test:potato_8"},
	interval = 10.0, -- Run every 10 seconds
	chance = 50, -- Select every 1 in 50 nodes
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "farm_test:soil_potato"})
	end
})

--======================--
-- leek 				--
--======================--

farming.register_plant("farm_test:leek", {
description = "Leek Seeds",
inventory_image = "farm_test_leek_seed.png",
steps = 8,
minlight = 13,
maxlight = LIGHT_MAX,
fertility = {"grassland"}
})
